import React, { useRef } from "react";
import {Animated, View, StyleSheet, PanResponder, Text, Image} from "react-native";

const MoveSquare = () => {
  const pan = useRef(new Animated.ValueXY()).current;

  const panResponder = useRef(
      PanResponder.create({
        onMoveShouldSetPanResponder: () => true,
        onPanResponderMove: Animated.event(
            [
              null,
              { dx: pan.x, dy: pan.y }
            ]
        ),
        onPanResponderRelease: () => {
          Animated.spring(
              pan, // Auto-multiplexed
              { toValue: { x: 0, y: 0 } } // Back to zero
          ).start();
        }
      })
  ).current;

  return (
      <View style={styles.container}>
        <Text style={styles.titleText}>This is a cube, Dragg it around!</Text>
        <Image
            style={styles.logo}
            source={require('./assets/down-arrow.png')}
        />
        <Animated.View
            style={{
              transform: [{ translateX: pan.x }, { translateY: pan.y }]
            }}
            {...panResponder.panHandlers}
        >
          <View style={styles.box} />
        </Animated.View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 230
  },
  titleText: {
    fontSize: 20,
    lineHeight: 24,
    fontWeight: "bold",
    color: "#EDEDED"
  },
  box: {
    marginTop: 20,
    height: 60,
    width: 60,
    backgroundColor: "#0ba0b3",
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 10,
  },
  logo: {
    marginTop: 20,
    height: 50,
    width: 50,
}
});

export default MoveSquare;